#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define NUM_THREADS 8

long sum = 0;

void *factorial(void *n_void);

int main(int argc, char* argv[]){
    clock_t t1;
    pthread_t inc_n_thread[NUM_THREADS];
    
    for (long i = 3; i <= 10; i++)
    {
        
        pthread_create(&inc_n_thread[i], NULL, &factorial, &i);
        pthread_join(inc_n_thread[i], NULL);
    }
    printf("La suma de todos los factoriales es %ld\n",sum);
    t1 = clock();
    printf("Tiempo de ejecucion %f\n",(t1/1000000.0F)*1000);

    return 0;
}

void *factorial(void *n_void){
    long *n = (long *)n_void;
    long total = 1;
    for (long i = 2; i <= *n; i++)
    {
        total = total * i;
    }
    printf("El factorial de %ld es %ld\n",*n,total);
    sum = sum + total;
    pthread_exit(NULL);
    return NULL;
}