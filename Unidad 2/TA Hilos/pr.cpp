#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define MAX_THREADS 8
#define VECTOR_SIZE 100000000

pthread_mutex_t mutex;
int private_count[100000][2];
long valorI;
long suma = 0;

void *hiloFactorizar(void *n_arg){
long *tam = (long *)n_arg;
long fact = 1;
    for(long j = 2; j <= *tam; j++)
   { 
    fact = fact * j;
   }
   printf("%ld valor %ld\n",*tam,fact);
   suma = suma + fact;

   return NULL;
}

int main(int argc, char* argv[]){
    
    clock_t t1;
    pthread_t tid[MAX_THREADS];
    printf("Ingrese la serie del factorial que desea calcular: ");
    scanf("%ld", &valorI);
    int numN = valorI;
    for(long i = 2; i<= numN;i++){
        pthread_create(&tid[i], NULL, &hiloFactorizar, &i);
        pthread_join(tid[i], NULL);
    }
    printf("suma %ld\n",suma);
    t1 = clock();
    printf("[3s-01] Elapsed time %f\n",(((float)t1) /1000000.0F) * 1000); 

    return 0;
}