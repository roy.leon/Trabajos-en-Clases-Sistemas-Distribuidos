import java.io.InputStreamReader;
import java.rmi.Naming;
import java.util.Scanner;

public class Cliente {
    public static void main(String[] args) {
        try {
            InterfazRmi interfaz = (InterfazRmi) Naming.lookup("rmi://localhost/oyente");
            System.out.println("Como te llamas?");
            Scanner escaner = new Scanner(new InputStreamReader(System.in));//TODO: handle exception
            System.out.println(interfaz.saludar(escaner.next()));
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
