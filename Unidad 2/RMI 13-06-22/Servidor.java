import java.rmi.Naming;

public class Servidor {
    public Servidor(){
        try{
            InterfazRmi objetoD = new ImplementacionRmi();
            Naming.rebind("rmi://localhost/oyente", objetoD);
        }catch(Exception ex){
            System.out.println(ex);
        }
    }
    public static void main(String[] args) {
        new Servidor();
    }
}
