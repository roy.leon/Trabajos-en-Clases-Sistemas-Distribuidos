import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.RemoteException;;

public class ImplementacionRmi extends UnicastRemoteObject implements InterfazRmi{
    public ImplementacionRmi() throws RemoteException{
        super();
    }

    public String saludar(String msj) throws RemoteException{
        return "Hola " + msj;
    }
}
