#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// definición de un candado para arbitrar el acceso a una variable compartida
pthread_mutex_t mutex;
#define MAX 100
// Vector de identificadores de hilos


int x = 0;
int y = 0;

void *inc_x(void *x_void_ptr);


int main(int argc, char* argv[]){
    clock_t t1, t2;

    printf("x: %d, y: %d\n",x,y);

    pthread_t inc_x_thread;

    pthread_create(&inc_x_thread, NULL ,&inc_x, &x);

    pthread_join(inc_x_thread,NULL);

    while (++y < MAX)
    {
        printf("y: %d\t",y);
    }
    t2 = clock();
    printf("y terminó el incremento\n");
    printf("X: %d, Y: %d\n",x,y);
 
    printf("Riempo de ejecición: %f\n",(((float)t2 - (float)t1) /1000000.0F) * 1000);
    
    return 0;
}

void *inc_x(void *x_void_ptr){
    int *x_ptr = (int *)x_void_ptr;
    printf("Estoy en el método implementado\n");
    while (++*x_ptr < MAX)
    {
        printf("x: %d\t",*x_ptr);
    }
    printf("x terminó el incremento\n");
    /*pthread_mutex_lock(&mutex);
	x = x + *x_ptr;
	pthread_mutex_unlock(&mutex);*/
    return NULL;
}