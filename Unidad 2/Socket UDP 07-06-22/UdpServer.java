import java.io.*;
import java.net.*;

public class UdpServer {
    public static void main(String[] args) throws Exception{
        int port = 9876;
        DatagramSocket serveSocket = new DatagramSocket(port);
        while(true){
            byte[] reciveData = new byte[1024];
            byte[] sendData = new byte[1024];
            System.out.println("SERVER ACTIVE");
            DatagramPacket recivePacket = new DatagramPacket(reciveData, reciveData.length);
            serveSocket.receive(recivePacket);
            String sentence = new String(recivePacket.getData());
            System.out.println(recivePacket.getAddress());
            System.out.println("Mensaje recivido: " + sentence);
            
            System.out.println("Enviando un dato al servidor");
            String sentenceSend = "HE RECIBIDO TU MENSAJE: " + sentence.toUpperCase();
            sendData = sentenceSend.getBytes();
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, recivePacket.getAddress(), recivePacket.getPort());
            serveSocket.send(sendPacket);
            System.out.println("SERVER OUT");
        }
    }
}
