import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UdpClient {
    public static void main(String[] args) throws Exception{
        DatagramSocket clientSocket = new DatagramSocket();
        BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));

        InetAddress ipAddress = InetAddress.getByName("127.0.0.1");
        byte[] reciveData = new byte[1024];
        byte[] sendData = new byte[1024];
        int port = 9876;

        System.out.println("Enviando un dato al servidor");
        String sentence = inFromUser.readLine();
        sendData = sentence.getBytes();

        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, ipAddress, port);
        System.out.println("-----" + new String(sendPacket.getData()));
        clientSocket.send(sendPacket);

        System.out.println("Esperando Mensaje del Server");
        DatagramPacket recivePacket = new DatagramPacket(reciveData, reciveData.length);
        clientSocket.receive(recivePacket);
        String sentenceRecive = new String(recivePacket.getData());
        //System.out.println(recivePacket.getAddress());
        System.out.println("Mensaje recivido: " + sentenceRecive);
        clientSocket.close();
    }   
}
