import java.io.*;
import java.net.*;

public class Servidor{
    static String host = "230.0.0.1";
    static int port = 5000;

    public static void main(String args[])throws IOException, ClassNotFoundException{
        byte[] buffer = new byte[1024];
        MulticastSocket multicastSocket = new MulticastSocket(port);
        InetAddress address = InetAddress.getByName(host);
        multicastSocket.joinGroup(address);
        
        while(true){
            DatagramPacket msgPacket = new DatagramPacket(buffer, buffer.length);
            multicastSocket.receive(msgPacket);
            String msg = new String(msgPacket.getData(), msgPacket.getOffset(), msgPacket.getLength());
            System.out.println("Mensaje recibido -> " + msg);
        }
    }
}