import java.io.*;
import java.net.*;

public class Cliente {
    static String host = "230.0.0.1";
    static int port = 5000;
    
    public static void main(String[] args)throws IOException, ClassNotFoundException, InterruptedException {
        
        int cont = 1;
        DatagramSocket socket = new DatagramSocket();
        InetAddress group = InetAddress.getByName(host);
        int id = (int)(Math.random()*1000+1);
        System.out.println("Id del dispositivo " + id);
        while (true) {
            String aux = "El dispositivo " + id + " envia el numero " + cont;
            byte[] msg = aux.getBytes();
            DatagramPacket packet = new DatagramPacket(msg, msg.length, group, port);
            socket.send(packet);
            System.out.println("Numero enviado " + cont);
            cont++;
            Thread.sleep(1000);
        }
        
    }
}
