import java.rmi.*;

public interface InterfazRmi extends Remote{
    public String retiro(Float cantidad) throws RemoteException;
    public String deposito(Float cantidad) throws RemoteException;
}
