import java.rmi.*;
import java.rmi.server.UnicastRemoteObject;

public class ImplementacionRmi extends UnicastRemoteObject implements InterfazRmi{
    public ImplementacionRmi() throws RemoteException{
        super();
    }

    public String retiro(Float cantidad) throws RemoteException{
        return "Ha realizado un retiro de $" + cantidad;
    }

    public String deposito(Float cantidad) throws RemoteException{
        return "Ha realizado un deposito de $" + cantidad;
    }
}
