import java.io.*;
import java.rmi.*;
import java.util.Scanner;

public class Cliente {
    public static void main(String[] args) {
        try {
            InterfazRmi interfaz = (InterfazRmi) Naming.lookup("rmi://localhost/banco");
            while (true) {
                System.out.println("Ingrese el numero de la accion a realizar\n1. Deposito\n2. Retiro");
                Scanner scanner1 = new Scanner(new InputStreamReader(System.in));
                int opcion = scanner1.nextInt();
                if (opcion == 1 || opcion == 2) {
                    System.out.println("Ingrese la cantidad");
                    Scanner scanner2 = new Scanner(new InputStreamReader(System.in));
                    Float cantidad = scanner2.nextFloat();
                    if (opcion == 1) {
                        System.out.println(interfaz.deposito(cantidad));
                    }else if (opcion == 2) {
                        System.out.println(interfaz.retiro(cantidad));
                    }
                } else {
                    System.out.println("Opcion no valida");
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
