#Elaborado por: Roy Emmanuel Leon Castillo
import time

nodos = [9998,10001,10005,10000]
#nodos = [9998,9998,9998,9998]
Dn = [0,0,0,0]
Dnprima = [0,0,0,0]
An = [0,0,0,0]
nodoPricipal = 4
t0 = 0
t1 = 0
periodoTiempo = 5

def sonIguales():
    global nodos
    for n in nodos:
        if n != nodos[0]:
            return False
    return True

def procesoIgular():
    global t0
    global t1
    global nodos
    global Dn
    global D
    t0 = nodos[nodoPricipal - 1]
    for i in range(5):
        print("Nodos -> ",str(nodos))
        time.sleep(1)
        sumarPeriodoTiempo()
        if i == 0:
            for j in range(len(Dn)):
                if j != (nodoPricipal - 1):
                    Dn[j] = nodos[j] - t0
            print("\tDn -> ",str(Dn))
        elif i == 1:
            t1 = nodos[nodoPricipal - 1]
            for j in range(len(Dnprima)):
                if j != (nodoPricipal - 1):
                    Dnprima[j] = Dn[j] - ((t1-t0)/2)
            D = sum(Dnprima)/len(nodos)
            nodos[nodoPricipal - 1]+=D
            for j in range(len(An)):
                if j != (nodoPricipal - 1):
                    An[j] = D - Dnprima[j]
            print("\tDn Prima -> ",str(Dnprima))
            print("\tD -> ",str(D))
            print("\tNodos Actualizados -> ",str(nodos))
            print("\tAn -> ",str(An))
        elif i == 2:
            for j in range(len(nodos)):
                if j != (nodoPricipal - 1):
                    nodos[j]+=An[j]
            print("\tNodos Actualizados -> ",str(nodos))

def sumarPeriodoTiempo():
    global periodoTiempo
    global nodos
    for i in range(len(nodos)):
        nodos[i]+=periodoTiempo

def main():
    iguales = sonIguales()
    while not iguales:
        procesoIgular()
        iguales = sonIguales()
    print("Tiempos igualados")

print("\tPeriodo de Tiempo -> ",periodoTiempo)
main()