#Elaborado por: Roy Emmanuel Leon Castillo
from asyncio.windows_events import NULL
import time

nodos = [[0,0,0,0],[0,0,0],[0,0,0,0]]
saltos = [[1,1,3,3],[1,2,2,1],[2,2,3,2],[3,1,1,3]] #[linea de partida, punto de partida, linea de llegada, punto de llegada]
mayorRango = 4
def main():
    global nodos
    #for k in range(mayorRango):
    for j in range(len(nodos[0])):
        if j != 0:
            for i in range(len(nodos)):
                if len(nodos[i]) == mayorRango:
                    if nodos[i][j] <= nodos[i][j-1]:
                        nodos[i][j] = nodos[i][j-1] + 1
                elif j < len(nodos[i]):
                    if nodos[i][j] <= nodos[i][j-1]:
                        nodos[i][j] = nodos[i][j-1] + 1
            print(nodos)
            print()
            saltar(j)
            time.sleep(1)
    print()

def saltar(n):
    global nodos
    #print(nodos)
    for i in range(len(saltos)):
        if saltos[i][1] == n:
            print("\t",str(saltos[i]))
            if(nodos[saltos[i][0]-1][saltos[i][1]] > nodos[saltos[i][2]-1][saltos[i][3]]):
                print("\tEnviado -> ",str(nodos[saltos[i][0]-1][saltos[i][1]]))
                print("\tActual -> ",str(nodos[saltos[i][2]-1][saltos[i][3]] + 1))
                nodos[saltos[i][2]-1][saltos[i][3]] = nodos[saltos[i][0]-1][saltos[i][1]] + 1
    print("\tActualizados -> ",str(nodos))
    print()

#print(nodos)
for i in range(len(nodos)):
    main()
print("Resultado final -> ",str(nodos))
lista = []
for j in range(len(nodos[0])):
    for i in range(len(nodos)):
        if j != 0:
            if len(nodos[i]) == mayorRango:
                lista.append((nodos[i][j] + ((i+1)/10)))
            elif j < len(nodos[i]):
                lista.append((nodos[i][j] + ((i+1)/10)))
lista.sort()
print("Ordenación de menor mayor")
for i in range(len(lista)):
    print(str((i+1)),"\t",str(lista[i]))