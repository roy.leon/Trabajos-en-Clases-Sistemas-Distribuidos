#Elaborado por: Roy Emmanuel Leon Castillo
import time

nodos = [[[0,0,0],[0,0,0],[0,0,0],[0,0,0]],[[0,0,0],[0,0,0],[0,0,0]],[[0,0,0],[0,0,0],[0,0,0],[0,0,0]]]
saltos = [[1,1,3,3],[1,2,2,1],[2,2,3,2],[3,1,1,3]] #[linea de partida, punto de partida, linea de llegada, punto de llegada]
mayorRango = 4
def main():
    global nodos
    #for k in range(mayorRango):
    for j in range(len(nodos[0])):
        if j != 0:
            for i in range(len(nodos)):
                if len(nodos[i]) == mayorRango:
                    aux = nodos[i][j-1].copy()
                    for k in range(len(nodos[i][j])):
                        if aux[k] > nodos[i][j][k]:
                            nodos[i][j][k] = aux[k]
                    if nodos[i][j][i] <= nodos[i][j-1][i]:
                        nodos[i][j][i] = nodos[i][j-1][i] + 1

                elif j < len(nodos[i]):
                    aux = nodos[i][j-1].copy()
                    for k in range(len(nodos[i][j])):
                        if aux[k] > nodos[i][j][k]:
                            nodos[i][j][k] = aux[k]
                    if nodos[i][j][i] <= nodos[i][j-1][i]:
                        nodos[i][j][i] = nodos[i][j-1][i] + 1


            for i in range(len(nodos)):
                print("\t ",str(nodos[i]))
            print("-------------------------------")
            saltar(j)
            #time.sleep(1)
    print()

def saltar(n):
    global nodos
    #print(nodos)
    for i in range(len(saltos)):
        if saltos[i][1] == n:
            print("\t",str(saltos[i]))
            if(nodos[saltos[i][0]-1][saltos[i][1]][saltos[i][0]-1] > nodos[saltos[i][2]-1][saltos[i][3]][saltos[i][0]-1]):
                k = saltos[i][3]
                while k < len(nodos[saltos[i][2]-1]):
                    if saltos[i][0] == 1:
                        nodos[saltos[i][2]-1][k][0] = nodos[saltos[i][0]-1][saltos[i][1]][0]
                        if nodos[saltos[i][2]-1][k][1] < nodos[saltos[i][0]-1][saltos[i][1]][1]:
                            nodos[saltos[i][2]-1][k][1] = nodos[saltos[i][0]-1][saltos[i][1]][1]
                        if nodos[saltos[i][2]-1][k][2] < nodos[saltos[i][0]-1][saltos[i][1]][2]:
                            nodos[saltos[i][2]-1][k][2] = nodos[saltos[i][0]-1][saltos[i][1]][2]
                    elif saltos[i][0] == 2:
                        nodos[saltos[i][2]-1][k][1] = nodos[saltos[i][0]-1][saltos[i][1]][1]
                        if nodos[saltos[i][2]-1][k][0] < nodos[saltos[i][0]-1][saltos[i][1]][0]:
                            nodos[saltos[i][2]-1][k][0] = nodos[saltos[i][0]-1][saltos[i][1]][0]
                        if nodos[saltos[i][2]-1][k][2] < nodos[saltos[i][0]-1][saltos[i][1]][2]:
                            nodos[saltos[i][2]-1][k][2] = nodos[saltos[i][0]-1][saltos[i][1]][2]
                    elif saltos[i][0] == 3:
                        nodos[saltos[i][2]-1][k][2] = nodos[saltos[i][0]-1][saltos[i][1]][2]
                        if nodos[saltos[i][2]-1][k][0] < nodos[saltos[i][0]-1][saltos[i][1]][0]:
                            nodos[saltos[i][2]-1][k][0] = nodos[saltos[i][0]-1][saltos[i][1]][0]
                        if nodos[saltos[i][2]-1][k][1] < nodos[saltos[i][0]-1][saltos[i][1]][1]:
                            nodos[saltos[i][2]-1][k][1] = nodos[saltos[i][0]-1][saltos[i][1]][1]
                    k+=1
                for i in range(len(nodos)):
                    print("\t -> ",str(nodos[i]))
    print("\tActualizados -> ")
    for i in range(len(nodos)):
        print("\t -> ",str(nodos[i]))
    print()

#print(nodos)
#for i in range(len(nodos)):
main()
print("Resultado final")
for i in range(len(nodos)):
    print(nodos[i])