import imp
from socket import *
import time
import datetime

def ConvertStringToHour(chain):
    format = "%Y-%m-%d   %H:%M:%f"
    chainHour = datetime.datetime.strptime(chain, format)
    return chainHour

IpServer = "localhost"
portServer = 8000
clientSocket = socket(AF_INET, SOCK_STREAM)#Conexion TCP

start = time.time()
clientSocket.connect((IpServer, portServer))

chainHour = clientSocket.recv(4096).decode()

end = time.time()
time = end - start

hour = ConvertStringToHour(chainHour)

print("El tiempo total de enviado y recibido fue -> ", time)
halfTime = time/2
print("El tiempo total de enviado fue -> ", halfTime)
print("Hora del servidor -> ", chainHour)
print("La hora exacta es -> ", hour + datetime.timedelta(seconds=halfTime))

clientSocket.close()