from multiprocessing import connection
from socket import *
from datetime import *
import socketserver

address = "localhost"
port = 8000

serverSocket = socket(AF_INET, SOCK_STREAM)#Conexion TCP

serverSocket.bind((address,port))
serverSocket.listen()

while(True):
    connectionSocket, addressClient = serverSocket.accept()
    print("Se estableció conexión con el Cliente ", addressClient)
    hour = datetime.now()
    chainHour = hour.strftime("%Y-%m-%d %H:%M:%f")
    print("Enviando hora al Cliente ", addressClient)
    print(hour)
    connectionSocket.send(chainHour.encode())
    connectionSocket.close()