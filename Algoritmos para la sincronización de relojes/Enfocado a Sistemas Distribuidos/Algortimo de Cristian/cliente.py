#Elaborado por Roy Emmanuel Leon Castillo
import socket
import time

HOST = "127.0.0.1"
PORT = 65432
cont = 38 
contTiempo = 0
peridoTiempo = 4
t0 = 0
t1 = 0
Cs = 0
C = 0
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    s.sendall(cont.to_bytes(2, 'big'))
    data = s.recv(1024)
    while True:
        print(cont)
        contTiempo+=1
        if contTiempo == peridoTiempo:
            s.sendall(cont.to_bytes(2, 'big'))
            print("Mensaje enviado al servidor  - > ",str(cont))
            t0 = cont
        elif contTiempo == ((peridoTiempo * 2) - 1 + (peridoTiempo - 1)):
            data = s.recv(1024)
            t1 = cont
            Cs = int.from_bytes(data, "big")
            print("Mensaje recibido del cliente - > ",str(Cs))
            # + 3 ya que se demora un segufundo en pasar
            C = Cs + ((t1 - t0)/2) + 3
            print("T0 -> ",str(t0))
            print("T1 -> ",str(t1))
            print("Cs -> ",str(Cs))
            print("C ",str(C))
            if C < cont:
                print("Retrasar")
                time.sleep((cont-C))
            elif C > cont:
                print("Actualizar a -> ",str(C))
                cont = C
        time.sleep(1)
        cont+=1