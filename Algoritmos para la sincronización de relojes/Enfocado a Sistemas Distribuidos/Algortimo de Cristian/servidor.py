#Elaborado por Roy Emmanuel Leon Castillo
import socket
import time

HOST = "127.0.0.1"
PORT = 65432
cont = 33 
contTiempo = 0
peridoTiempo = 4
Cs = 0
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    conn, addr = s.accept()
    with conn:
        
        print(f"Connected by {addr}")
        
        data = conn.recv(1024)
        pasa = True
        if data:
            pasa = True
            conn.sendall(data)
        while pasa:
            print(cont)
            contTiempo+=1
            if contTiempo == peridoTiempo:
                data = conn.recv(1024)
                print("Mensaje recibido del cliente - > ",str(int.from_bytes(data, "big")))
                Cs = cont
                print("Mensaje enviado al cliente  - > ",str(Cs))
            elif contTiempo == ((peridoTiempo * 2) - 1 ):
                #print("Hola")
                conn.sendall(Cs.to_bytes(2, 'big'))
            time.sleep(1)
            cont+=1