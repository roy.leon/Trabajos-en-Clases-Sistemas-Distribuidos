from dateutil import parser
import threading
import datetime
import socket
import time

Cc = 0
periodoTiempo = 5
contTiempo = 0
Dn = 0

def inicarHoraEnvio(cliente):
    global Cc
    global contTiempo
    global periodoTiempo
    while True:
        if contTiempo == ((periodoTiempo*5)):
            break
        else:
            if contTiempo == ((periodoTiempo*3)):
                #print(periodoTiempo*3)
                #print(contTiempo)
                enviar = Dn
                
                cliente.send(str(enviar).encode())
                print("Dato Enviado -> ", Dn,end = "\n\n")
            time.sleep(periodoTiempo)

def inicarHoraRecibo(cliente):
    empesar = False
    global Cc
    global contTiempo
    global periodoTiempo
    global Dn
    horaRecibida = int(cliente.recv(1024).decode())
    while True:
        if empesar or (horaRecibida == 0):
            print("Reloj del Cliente - > ",str(Cc))
            Cc+=periodoTiempo
            empesar = True
            #print("La hora sincronizada en el cliente es: " + str(horaRecibida), end = "\n\n")
        if contTiempo == ((periodoTiempo*5)):
            break
        else:
            if contTiempo == (periodoTiempo*3):
                horaRecibida = int(cliente.recv(1024).decode())
                print("Dato Recibido: " + str(horaRecibida), end = "\n\n")
                Cc = Cc + horaRecibida
                Dn = 0
            elif contTiempo == periodoTiempo:
                horaRecibida = int(cliente.recv(1024).decode())
                print("Dato Recibido: " + str(horaRecibida), end = "\n\n")
                #print(horaRecibida - periodoTiempo)
                #print(Cc)
                Dn = (Cc - periodoTiempo) - horaRecibida
        contTiempo+=periodoTiempo
        time.sleep(periodoTiempo)
        

def iniciarCliente(port = 8080):
    global Cc
    Cc = int(input("Ingrese el reloj del servidor "))
    print(Cc)
    cliente = socket.socket()         
    cliente.connect(('127.0.0.1', port))

    print("Empezando a recibir la hora del servidor\n")
    enviarHiloTiempo = threading.Thread(target = inicarHoraEnvio, args = (cliente, ))
    enviarHiloTiempo.start()

    print("Empezando a recibir la hora sincronizada del servidor\n")
    recibirHiloTiempo = threading.Thread(target = inicarHoraRecibo, args = (cliente, ))
    recibirHiloTiempo.start()
 
if __name__ == '__main__':
    iniciarCliente(port = 8080)