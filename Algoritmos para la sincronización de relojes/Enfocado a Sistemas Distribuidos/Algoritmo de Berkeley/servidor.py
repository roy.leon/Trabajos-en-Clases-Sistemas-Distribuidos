from turtle import st
from dateutil import parser
import threading
import socket
import time
 
datosCliente = {}
Cs = 0
nClientes = 3
periodoTiempo = 5
contTiempo = 0
t0 = 0

def RecibirHoraReloj(conexion, direccion):
    global Cs
    global contTiempo
    global periodoTiempo
    global t0
    global Cs
    datosCliente[direccion] = {
                        "tiempoReloj"      : 0.0,
                        "diferenciaTiempo" : 0.0,
                        "conexion"       : conexion
                        }
    while True:
        if contTiempo == ((periodoTiempo*5)):
            break
        else:
            if contTiempo == ((periodoTiempo*3)):
                #print(periodoTiempo*3)
                #print(contTiempo)
                
                tiempoReloj = int(conexion.recv(1024).decode())
                print("Recibir -> ",str(tiempoReloj))
                diferenciaTiempoReloj = tiempoReloj - (((Cs - periodoTiempo) - t0)/2)
                print("t0 ", str(t0))
                print("t1 ", str((Cs - periodoTiempo)))
                print("Dn Prima ", str(diferenciaTiempoReloj))
        
                datosCliente[direccion] = {
                            "tiempoReloj"      : tiempoReloj,
                            "diferenciaTiempo" : diferenciaTiempoReloj,
                            "conexion"       : conexion
                            }
    
                print("Datos del cliente actualizados con: "+ str(direccion), end = "\n\n")
        
 
def empezarConexion(servidor):
    while True:
        conexionClienteServidor, direccion = servidor.accept()
        clienteDireccion = str(direccion[0]) + ":" + str(direccion[1])
 
        print(clienteDireccion + " se ha conectado con éxito")
 
        hilo = threading.Thread(
                         target = RecibirHoraReloj,
                         args = (conexionClienteServidor,clienteDireccion, ))
        hilo.start()

def obtenerMediaDiferenciaReloj():
    datosClienteActuales = datosCliente.copy()
 
    listaDiferenciaTiempo = list(client['diferenciaTiempo'] for direccionCliente, client in datosCliente.items())
                                    
    sumDiferenciaReloj = sum(listaDiferenciaTiempo, Cs)
    mediaDiferenciaReloj = sumDiferenciaReloj / len(datosCliente)
 
    return  mediaDiferenciaReloj

def sincronizarTodosRelojes():
    global Cs
    global contTiempo
    global periodoTiempo
    global t0
    empezar = True
    sePuedeEnviar = False
    while True:
        
        print("Nuevo ciclo de sincronización iniciado.")
        print("Número de clientes a sincronizar: " + str(len(datosCliente)))
        #print(contTiempo)
        if contTiempo == ((periodoTiempo*5)):
            break
        else:
            if len(datosCliente) == nClientes and sePuedeEnviar:
                print("\tReloj del Servidor - > ",str(Cs))
                
                mediaDiferenciaReloj = obtenerMediaDiferenciaReloj()
                if contTiempo == (periodoTiempo*3):
                    suma = 0
                    for direccionCliente, cliente in datosCliente.items():
                        print(direccionCliente)
                        suma = suma + int(cliente['diferenciaTiempo'])
                    print("Suma ",str(suma))
                    D = round(suma/(nClientes + 1))
                    Cs = Cs + D
                    print("D ",str(D))
                    for direccionCliente,  cliente in datosCliente.items():
                        try:
                            An = D - int(cliente['diferenciaTiempo'])
                            cliente['conexion'].send(str(An).encode())
                        except Exception as e:
                            print("Ocurrio un error al enviar la hora sincronizada a través de " + str(direccionCliente))
                if contTiempo == periodoTiempo:
                    enviar = Cs - periodoTiempo
                    t0 = enviar
                    print("Enviado -> ",str(enviar))
                    for direccionCliente, cliente in datosCliente.items():
                        try:
                            #print(direccionCliente)
                            #print(cliente)
                            tiempoSincronizado = Cs + mediaDiferenciaReloj
                            cliente['conexion'].send(str(enviar).encode())
                        except Exception as e:
                            print("Ocurrio un error al enviar la hora sincronizada a través de " + str(direccionCliente))
                
                Cs+=periodoTiempo
                contTiempo+=periodoTiempo
            elif len(datosCliente) == nClientes and not sePuedeEnviar:
                sePuedeEnviar = True
                print("Reloj del Servidor - > ",str(Cs))
                enviar = Cs
                if empezar:
                    enviar = 0
                    empezar = False
                mediaDiferenciaReloj = obtenerMediaDiferenciaReloj()
                for direccionCliente, cliente in datosCliente.items():
                    try:
                        tiempoSincronizado = Cs + mediaDiferenciaReloj
                        cliente['conexion'].send(str(enviar).encode())
                    except Exception as e:
                        print("Ocurrio un error al enviar la hora sincronizada a través de " + str(direccionCliente))
                Cs+=periodoTiempo
                contTiempo+=periodoTiempo
            else:
                print("No hay datos del cliente. Sincronización no aplicable.")
            print("\n\n")
        
        time.sleep(periodoTiempo)
        

def inicarRelojServidor(port = 8080):
    global Cs
    Cs = int(input("Ingrese el reloj del servidor "))
    #print(Cs)
    servidor = socket.socket()
    servidor.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
 
    print("Socket en el servidor se ha creado con éxito\n")
       
    servidor.bind(('', port))

    servidor.listen(10)
    print("El servidor del reloj comenzó...\n")

    print("Empezando a hacer conexiones...\n")
    hiloMaestro = threading.Thread(target = empezarConexion, args = (servidor, ))
    hiloMaestro.start()

    print("Iniciando la sincronización en paralelo...\n")
    hiloSincronizado = threading.Thread(target = sincronizarTodosRelojes, args = ())
    hiloSincronizado.start()

if __name__ == '__main__':
    inicarRelojServidor(port = 8080)