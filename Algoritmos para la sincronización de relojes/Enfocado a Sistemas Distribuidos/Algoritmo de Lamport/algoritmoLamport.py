#Algoritmo de lamport
import threading 
import time

TIEMPOESPERA = 1

NUMEROS_HILOS = 10

numero = [0] * NUMEROS_HILOS
elegirProceso = [False] * NUMEROS_HILOS
secccionCritica = [False] * NUMEROS_HILOS 

def Maximo():
    max = -1
    for i in numero:
        if (i > max):
            max = i 
    return max

def Bloqueado(i):
    elegirProceso[i] = True
    numero[i] = 1 + Maximo()
    elegirProceso[i] = False

    for j in range(NUMEROS_HILOS):
        while elegirProceso[j]:
            continue
        while ((numero[j] != 0) and (numero[j] < numero[i] or (numero[j] == numero[i]) and (j < i))):
            continue

def desBloqueado(i):
    numero[i] = 0 

def EjecutarHilo(i):
            Bloqueado(i)
            print("Hilo " + str(i) + " esta en la seccion critica\n")
            print("Proceso " + str(i) + " -> A")
            print("Proceso " + str(i) + " -> B")
            print("Proceso " + str(i) + " -> C")
            secccionCritica[i] = True
            time.sleep(TIEMPOESPERA)
            secccionCritica[i] = False
            desBloqueado(i)
            print("Hilo " + str(i) + " esta fuera de la seccion critica\n")
            print("Proceso " + str(i) + " -> D")
            print("Proceso " + str(i) + " -> E")
            time.sleep(TIEMPOESPERA)
                
def Supervisar():
        tiempo = 1
        for i in range(NUMEROS_HILOS):
                print("-----------------------------")
                print ("Tiempo: " + str(tiempo))
                #for i in range(len(secccionCritica)):
                #   print(str(i) + " " + str(secccionCritica[i]))
                print(secccionCritica)
                tiempo+=1
                time.sleep(1)

def main():
        for num_hilo in range(NUMEROS_HILOS):
                hilo = threading.Thread(name='%s' %num_hilo, target=EjecutarHilo, args=(num_hilo,))    
                hilo.start()

main()
Supervisar()

#La información en unas ocasiones está desactualizada debido al tiempo que ocupara cada hilo