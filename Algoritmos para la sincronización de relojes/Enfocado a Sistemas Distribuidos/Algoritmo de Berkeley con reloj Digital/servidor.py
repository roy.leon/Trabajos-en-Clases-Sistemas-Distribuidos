from dateutil import parser
import threading
import datetime
import socket
import time
 
datosCliente = {}

def RecibirHoraReloj(conexion, direccion):
 
    while True:
        tiempoRelojString = conexion.recv(1024).decode()
        tiempoReloj = parser.parse(tiempoRelojString)
        diferenciaTiempoReloj = datetime.datetime.now() - tiempoReloj
 
        datosCliente[direccion] = {
                       "tiempoReloj"      : tiempoReloj,
                       "diferenciaTiempo" : diferenciaTiempoReloj,
                       "conexion"       : conexion
                       }
 
        print("Datos del cliente actualizados con: "+ str(direccion), end = "\n\n")
        time.sleep(5)
 
def empezarConexion(servidor):
    while True:
        conexionClienteServidor, direccion = servidor.accept()
        clienteDireccion = str(direccion[0]) + ":" + str(direccion[1])
 
        print(clienteDireccion + " se ha conectado con éxito")
 
        hilo = threading.Thread(
                         target = RecibirHoraReloj,
                         args = (conexionClienteServidor,clienteDireccion, ))
        hilo.start()

def obtenerMediaDiferenciaReloj():
    datosClienteActuales = datosCliente.copy()
 
    listaDiferenciaTiempo = list(client['diferenciaTiempo'] for direccionCliente, client in datosCliente.items())
                                    
    sumDiferenciaReloj = sum(listaDiferenciaTiempo, datetime.timedelta(0, 0))
    mediaDiferenciaReloj = sumDiferenciaReloj / len(datosCliente)
 
    return  mediaDiferenciaReloj

def sincronizarTodosRelojes():
    while True:
        print("Nuevo ciclo de sincronización iniciado.")
        print("Número de clientes a sincronizar: " + str(len(datosCliente)))
 
        if len(datosCliente) > 0:
            mediaDiferenciaReloj = obtenerMediaDiferenciaReloj()
            for direccionCliente, cliente in datosCliente.items():
                try:
                    tiempoSincronizado = datetime.datetime.now() + mediaDiferenciaReloj
                    cliente['conexion'].send(str(tiempoSincronizado).encode())
                except Exception as e:
                    print("Ocurrio un error al enviar la hora sincronizada a través de " + str(direccionCliente))
        else :
            print("No hay datos del cliente. Sincronización no aplicable.")
        print("\n\n")
        time.sleep(5)

def inicarRelojServidor(port = 8080):
    servidor = socket.socket()
    servidor.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
 
    print("Socket en el servidor se ha creado con éxito\n")
       
    servidor.bind(('', port))

    servidor.listen(10)
    print("El servidor del reloj comenzó...\n")

    print("Empezando a hacer conexiones...\n")
    hiloMaestro = threading.Thread(target = empezarConexion, args = (servidor, ))
    hiloMaestro.start()

    print("Iniciando la sincronización en paralelo...\n")
    hiloSincronizado = threading.Thread(target = sincronizarTodosRelojes, args = ())
    hiloSincronizado.start()

if __name__ == '__main__':
    inicarRelojServidor(port = 8080)