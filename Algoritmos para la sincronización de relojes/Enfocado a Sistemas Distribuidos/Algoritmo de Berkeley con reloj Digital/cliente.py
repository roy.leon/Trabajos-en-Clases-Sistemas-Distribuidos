from dateutil import parser
import threading
import datetime
import socket
import time

def inicarHoraEnvio(cliente):
    while True:
        cliente.send(str(datetime.datetime.now()).encode())
        print("Hora más reciente enviada con éxito", end = "\n\n")
        time.sleep(5)

def inicarHoraRecibo(cliente):
    while True:
        horaSincronizada = parser.parse(cliente.recv(1024).decode())
 
        print("La hora sincronizada en el cliente es: " + str(horaSincronizada), end = "\n\n")

def iniciarCliente(port = 8080):
    cliente = socket.socket()         
    cliente.connect(('127.0.0.1', port))

    print("Empezando a recibir la hora del servidor\n")
    enviarHiloTiempo = threading.Thread(target = inicarHoraEnvio, args = (cliente, ))
    enviarHiloTiempo.start()

    print("Empezando a recibir la hora sincronizada del servidor\n")
    recibirHiloTiempo = threading.Thread(target = inicarHoraRecibo, args = (cliente, ))
    recibirHiloTiempo.start()
 
if __name__ == '__main__':
    iniciarCliente(port = 8080)